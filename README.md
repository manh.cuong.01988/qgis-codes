## Here is my code using QGIS and Google Earth Engine

- The file UNEP-WCMC.py is a simple example of using Google Earth Engine in QGIS
- The file latlong_WRS2.py returns the list of all paths/rows containing the give point. We need to know the path/row if we use ee.Image(), since they are included into the Landsat file name.
- Here is the link to my repo which update Cluz3 (QGIS plugin) to allow Marxan to be put in another drive (e.g. D or E) to C on Windows:
https://github.com/kirimaru-jp/CLUZ_QGIS3

Simply, we just need to chance to that drive before running Marxan, by running:
batWriter.writerow([ os.path.splitdrive(marxanFullName)[0] ])
in the function "makeMarxanBatFile" of the file "cluz_functions5.py".
