# This script is a simple example of using Google Earth Engine in QGIS.
# Here we capture the landscape around UNEP-WCMC with coordinates (52.219646, 0.091925) using Landsat 8 satelite.

# First, one needs to import Google Earth Engine, through ee package
import ee
from ee_plugin import Map


# Then, depends on the purposes of use, one define the visualization parameters.
# There are several configurations for accentuate different objects
# For example, the combination of bands 4, 3, 2 make the layer look close to real
vizParams432 = {
  'bands': ['B4', 'B3', 'B2'],
  'min': 0,
  'max': 0.7,
  'gamma': 1.6
}

# While the combination of bands 7, 6, 4 accentuate the city region
vizParams764 = {
  'bands': ['B7', 'B6', 'B4'],
    'min': 0,
    'max': 0.7,
    'gamma': 1.6
}


# Define the coordinates of the location, e.g. UNEP-WCMC center
lat, lon = 52.219646, 0.091925

# Then one selects the date around the location. Note that the data is not available for every day, 
# hence one need to verify, e.g. using the following tool from Google Earth Engine
# [https://code.earthengine.google.com/?scriptPath=Examples%3ADatasets%2FLANDSAT_LC08_C01_T1_RT_TOA]
# One then obtain the collection of images
imgs = ee.ImageCollection('LANDSAT/LC08/C01/T1_RT_TOA')\
.filterBounds(ee.Geometry.Point(lon, lat))\
.filterDate('2017-01-17', '2017-01-18')

# Select one image from the collection, e.g. the first one
img = imgs.first()

#Center the map and display the images by adding different layers
Map.centerObject(img, 9)
Map.addLayer(img, vizParams432, '4-3-2')
Map.addLayer(img, vizParams764, '7-6-4')

# Then the layers can be saved by the QGIS menu Project > Import/Export > Export Map to Image.
