# -*- coding: utf-8 -*-
"""
@author: Cuong

Besides ee.ImageCollection() in Google Earth Engine, one can also download the satellite images using ee.Image().
However, to download, one needs the file names of Landsat images, which is
    LXS PPPRRR YYYYDDD GSIVV, where
    - L = Landsat
    - X = Sensor
    - S = Satellite (from 1 to 8)
    - PPP = WRS path
    - RRR = WRS row
    - YYYY = Year
    - DDD = Julian day of the year 
    - GSI = Ground station identifier 
    - VV = Archive version number 
For more details, please see at: https://gisgeography.com/landsat-file-naming-convention/

This script is based on https://github.com/robintw/LatLongToWRS, to find Landsat WRS-2 path(s)/row(s) which contain the give point with known longitude, latitude
Here we make some improvement:
    - Fix errors that make LatLongToWRS did not work with the new version of gdal.
    - Use only one data file - WRS2_polygons.pickle, instead of 8 files used by LatLongToWRS.
    - Use functional programming instead of object-oriented programming

"""

import pickle

# Load the polygon data made of path/row
polygons_file = "data/WRS2_polygons.pickle"
# Load data from disc
with open(polygons_file, "rb") as poly_file:
    loaded_polygon = pickle.load(poly_file)

import shapely.geometry

# Function to return the lists of all polygons which contain the coordinate of the give point, because the polygons could overlap
# Input:
#     - lat: latitude of the input point
#     - lon: longitude of the input point
#     - the polygons data file made of paths/rows
# Output:
#     - return the list of all Landsat WRS-2 path/row which contains the points 
def latlong_WRS2(lat, lon, polygs):
    pt = shapely.geometry.Point(lon, lat)
    # Init list of polygons from the data containing the point
    res = []
    # Iterate through every polygon
    for poly in polygs:
        # If the point is within the polygon then
        # append the current path/row to the results list
        if pt.within(poly[0]):
            res.append({'path': poly[1], 'row': poly[2]})
    return res

# For example, the coordinate of the UNEP-WCMC center
lat, lon = 52.219646, 0.091925

# Return the list of all Landsat WRS-2 paths/rows which contain the UNEP-WCMC center
latlong_WRS2(lat, lon, loaded_polygon)

# Extract the first path and row, p and r
p = latlong_WRS2(lat, lon, loaded_polygon)[0]['path']
r = latlong_WRS2(lat, lon, loaded_polygon)[0]['row']

